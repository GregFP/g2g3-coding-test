﻿
namespace SkeletonCode.CardGame
{
    class Card : ICard
    {
        public override string ToString()
        {
            string pips = "";
            switch (Value)
            {
                case 1: pips = "Ace";
                    break;
                case 11: pips = "Jack";
                    break;
                case 12: pips = "Queen";
                    break;
                case 13: pips = "King";
                    break;
                default:
                    pips = Value.ToString();
                    break;
            }
            return pips + " of " + Suit;
        }

        public string Suit { get ;  set; }

        public int Value { get;  set; }
    }
}
