﻿
namespace SkeletonCode.CardGame
{
	public class PackOfCardsCreator : IPackOfCardsCreator
	{
		public IPackOfCards Create()
		{
            PackOfCards pack = new PackOfCards();
            Card diamond;
            Card heart;
            Card spade;
            Card club;

            for (int value = 1; value < 14; value++)
            {
                diamond = new Card();
                heart = new Card();
                spade = new Card();
                club = new Card();

                diamond.Suit = "Diamonds";
                heart.Suit = "Hearts";
                spade.Suit = "Spades";
                club.Suit = "Clubs";
                diamond.Value = heart.Value = spade.Value = club.Value = value;
                pack.Add(diamond);
                pack.Add(heart);
                pack.Add(spade);
                pack.Add(club);
            }
            return pack;
		}
	}
}
