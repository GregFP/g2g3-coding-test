﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace SkeletonCode.CardGame
{
    class PackOfCards : IPackOfCards
    {
        List<Card> deck;
        public ReadOnlyCollection<Card> Deck { get; private set; }

        public PackOfCards()
        {
            deck = new List<Card>();
            Deck = new ReadOnlyCollection<Card>(deck);
        }

        internal void Add(Card card)
        {
            deck.Add(card);
        }
        
        /// <summary>
        /// Randomly alter the order of the cards in the pack.
        /// </summary>
        public void Shuffle()
        {
            // Pick 2 cards at random and swap them.  Do this sufficient times.
            Random rand = new Random();
            int count = deck.Count;
            Card cardTemp;
            for (int i = 0; i < 10 * count; i++)
            {
                int position1 = rand.Next(count);  // If you have 52 cards, this is a number from 0 to 51 inclusive.
                int position2 = rand.Next(count);
                cardTemp = deck[position1];
                deck[position1] = deck[position2];
                deck[position2] = cardTemp;
            }
        }

        /// <summary>
        /// Get the card from the top of the pack, and remove it from the pack.
        /// </summary>
        /// <returns>An ICard</returns>
        public ICard TakeCardFromTopOfPack()
        {
            Card card = deck[0];
            deck.Remove(card);
            return card;
        }

        public int Count
        {
            get { return this.Count(); }
        }

        //Generic enumerator required to implement the interface.
        public IEnumerator<ICard> GetEnumerator()
        {
           // Should be able to use the enumerator of the class we are wrapping.
            return deck.GetEnumerator();
        }

        // The non-generic enumerator calls the generic one.
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
