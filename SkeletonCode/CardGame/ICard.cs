﻿
namespace SkeletonCode.CardGame
{
	public interface ICard
	{
        string Suit { get; set; }
        int Value { get; set; }
    }
}
