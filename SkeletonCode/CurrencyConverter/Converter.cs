﻿using System.Collections.Generic;
using System;
namespace SkeletonCode.CurrencyConverter
{
	public class Converter
	{
		public decimal Convert(string inputCurrency, string outputCurrency, decimal amount)
		{
            //Fairly generic solution that can easily be extended for more codes and rates.
            
            string[] codes = {"GBP", "USD"}; 
            int numCodes = codes.Length;
            decimal[,] rates = new decimal[numCodes,numCodes];// All array elements will be initialised to 0M

            // Define the rates going from the 1st currency to the 2nd currency.
            // Need to define all the rates for one direction here.
            rates[0, 1] = 1.25M;

            // Define all the rates for going in the reverse direction.  Also allows for conversion from a currency to itself.

            for (int from = 0; from < numCodes; from++)
            {
                for (int to = from; to < numCodes; to++)
                {
                    if (from == to)
                    {
                        rates[from, to] = 1M;
                    }
                    else
                    {
                        if (rates[from, to] == 0M)
                        {
                            throw new ArgumentException("Rate not defined for " + codes[from] + " to " + codes[to]);
                        }
                        rates[to, from] = 1M / rates[from, to]; 
                    }
                }
            }

            int inputIndex = Array.IndexOf(codes, inputCurrency);
            if (inputIndex < 0)
            {
                throw new ArgumentException("Currency code not defined", inputCurrency);
            }
            
            int outputIndex = Array.IndexOf(codes, outputCurrency);
            if (outputIndex < 0)
            {
                throw new ArgumentException("Currency code not defined", outputCurrency);
            }
            
            amount *= rates[inputIndex, outputIndex];

            return amount;
		}
	}
}
