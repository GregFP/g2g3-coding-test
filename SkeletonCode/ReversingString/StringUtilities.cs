﻿using System.Text;

namespace SkeletonCode.ReversingString
{
	public class StringUtilities
	{
		public string Reverse(string input)
		{
			string output = string.Empty;

            if (input == null) return output;

            StringBuilder reverse = new StringBuilder();

			for(int i = input.Length - 1; i >= 0; i--)
			{
				reverse.Append(input[i]);
			}
			return reverse.ToString();
		}
	}
}
