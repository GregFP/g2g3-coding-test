﻿using System;

namespace SkeletonCode.CardGame
{
    class Game
    {
        static void Main(string[] args)
        {
            // Console executable to test out the card game methods.
            PackOfCardsCreator dealer = new PackOfCardsCreator();
            IPackOfCards pack = dealer.Create();
            ListCards(pack);

            Console.WriteLine("\nShuffle:\n");
            pack.Shuffle();
            ListCards(pack);

            Console.WriteLine("\nDeal card: " + pack.TakeCardFromTopOfPack());
            Console.WriteLine("\nRemaining cards:\n");
            ListCards(pack);
        }

        static void ListCards(IPackOfCards deck)
        {
            foreach (ICard card in deck)
            {
                Console.WriteLine(card);
            }
        }
    }
}
