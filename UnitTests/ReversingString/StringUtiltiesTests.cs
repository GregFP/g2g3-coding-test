﻿//using NUnit.Framework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SkeletonCode.ReversingString;

namespace UnitTests.ReversingString
{
	//[TestFixture]
    [TestClass]
	public class StringUtiltiesTests
	{

        [TestMethod]
        public void ReverseStringShouldReturnTheStringInTheReverseOrderEmptyStrings()
        {
            string input = "", expectedResult = "";
            StringUtilities stringUtilities = new StringUtilities();
            string result = stringUtilities.Reverse(input);

            Assert.AreEqual(expectedResult, result);
        }
        
        [TestMethod]
        public void ReverseStringShouldReturnTheStringInTheReverseOrderNormalStrings()
        {
            string input = "skeleton", expectedResult = "noteleks";
            StringUtilities stringUtilities = new StringUtilities();
            string result = stringUtilities.Reverse(input);

            Assert.AreEqual(expectedResult, result);
        }
        
        //[TestCase("", "")]
		//[TestCase("skeleton", "noteleks")]
		//[TestCase(null, "")]
        [TestMethod]
		public void ReverseStringShouldReturnTheStringInTheReverseOrderNullInput()
		{
            string input = null, expectedResult = "";
            StringUtilities stringUtilities = new StringUtilities();
			string result = stringUtilities.Reverse(input);

			Assert.AreEqual(expectedResult, result);
		}
    }
}
